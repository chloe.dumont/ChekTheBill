pragma solidity ^0.4.2;

import "./ConvertLib.sol";

contract CheckTheBill {

  // Variables for the beginning of the contract
  uint public deadline;
  uint nb_participants;
  address[] public  addresses;
  mapping (address => uint) public  balances;

  //Variables for the new expense
  string name;
  uint amount;
  address buyer;
  address[] public consumers;


  function CheckTheBill(){
       nb_participants=0;
       deadline=0;
       amount=0;
   }

 // now en secondes depuis le 1er janv 1970 à 0h donc conversion _lenght en secondes
  function setdays(uint _duration){
     deadline = now + _duration;
  }

	function getlimit() returns (uint){
		return deadline;
	}

 function AddressArray(address[] addresses_) {
          addresses = addresses_;
 }

  function getNumberOfAddresses()  returns (uint){
     nb_participants = addresses.length;
     return nb_participants;
  }

  function getAddressArray()  returns (address[]) {
      return addresses;
  }


  function balances_initiate(address addr, uint _amount)  constant returns(uint){
          balances[addr]=_amount;
          return balances[addr];
  }

  function getbalances(address addr) constant returns (uint){
      return balances[addr];
    }


  modifier checkbalance(address _buyer, uint _amount){
      require (balances[_buyer] > _amount);
      _;
  }

// fonction initialement prévue avec l'appel du modifier 'checkbalance' pour vérifier que la balance du payeur est bien sup au montant de la transaction
//// ---> function newexpense(uint _amount, string _name, address _buyer) checkbalance(_buyer, _amount)

  function newexpense(uint _amount, string _name, address _buyer) {
      balances[_buyer]-= _amount;
      name= _name;
  }

  event CashBack(address _buyer, address[] _consumers, uint _amount);

  function redistribution( address _buyer, address[] _consumers, uint _amount) constant returns (uint) {
    uint count = _amount/(_consumers.length);
     for (uint i = 0; i < _consumers.length; i++) {
          balances[_consumers[i]] -= count;
      }
      CashBack(_buyer, _consumers, count);
      balances[_buyer] += count*_consumers.length;
      return balances[_buyer];
  }


}
