var ConvertLib = artifacts.require("./ConvertLib.sol");
var CheckTheBill =  artifacts.require("./CheckTheBill.sol");

module.exports = function(deployer) {
  deployer.deploy(ConvertLib);
  deployer.link(ConvertLib, CheckTheBill);
  deployer.deploy(CheckTheBill);
};
