// Import the page's CSS. Webpack will know what to do with it.
import "../stylesheets/app.css";

// Import libraries we need.
import { default as Web3} from 'web3';
import { default as contract } from 'truffle-contract'

// Import our contract artifacts and turn them into usable abstractions.
import CheckTheBill_artifacts from '../../build/contracts/CheckTheBill.json'
var CheckTheBill = contract(CheckTheBill_artifacts);

// The following code is simple to show off interacting with your contracts.
// As your needs grow you will likely need to change its form and structure.
// For application bootstrapping, check out window.addEventListener below.
var accounts;
var account;
var i = 0;
//Array of the participants of the counts
var participant_array=Array();
var time=false;

window.App = {
//Function that will hide the block when button start pushed
  hide: function(id) {
    var elt = document.getElementById(id);
    if(elt.style.display=="none") {
      elt.style.display = "block";
      document.getElementById('hideBtn').innerHTML = "Start";
    } else {
      elt.style.display = "none";
      document.getElementById('hideBtn').innerHTML = "Settings";
    }

  },

  start: function() {
    var self = this;
    // Bootstrap the MetaCoin abstraction for Use.
    CheckTheBill.setProvider(web3.currentProvider);
    // Get the initial account balance so it can be displayed.
    web3.eth.getAccounts(function(err, accs) {
      if (err != null) {
        alert("There was an error fetching your accounts.");
        return;
      }

      if (accs.length == 0) {
        alert("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
        return;
      }

      accounts = accs;
      account = accounts[0];
      self.refreshparticipants();
      self.refreshdays();

    });
  },

  setStatus: function(message) {
  var status = document.getElementById("status"); //juste pour changer la valeur
  status.innerHTML = message;
  },

// ENTER the number of days of the trip
  setday:function(){
      var self = this;
      var days = parseInt(document.getElementById("day").value);
      // CONVERSION of  _lenght in secondes because NOW is in secondes
      days = days*3600*24;
      var meta;
      CheckTheBill.deployed().then(function(instance) {
        meta = instance;
        return meta.setdays(days, {from: account});
      }).then(function(res) {
        console.log("days transfered!")
        time=true;
        self.refreshdays();
      }).catch(function(e) {
        console.log(e);
        console.log("Error getting number see log.");
      });
    },

// TEST IF DAYS SET IS OK
  refreshdays:function(){
      var self = this;
      var meta;
      CheckTheBill.deployed().then(function(instance) {
        meta = instance;
        return meta.getlimit.call(account, {from: account});
      }).then(function(value) {

        while(time==true){
          var deadline = value.valueOf();
          // GET TIME FROM 1970 IN SECONDS
          var now= Math.round(new Date().getTime()/1000);
          if(now> deadline && time==true){
            self.hide("main");
             $("#end").toggleClass("show");
             time=false;
          }
        }

      }).catch(function(e) {
        console.log(e);
        self.setStatus("Error SHOWING lenght; see log.");
      });
    },









// GET the number of participants by counting the number of ADDRESSES  un the array via array.lenght
    refreshparticipants: function(){
      var self = this;
      var meta;
      CheckTheBill.deployed().then(function(instance) {
        meta = instance;
        return meta.getNumberOfAddresses.call(account, {from: account});
      }).then(function(value) {
        var nbparticipant = document.getElementById("nbparticipants");
        nbparticipant.innerHTML = value.valueOf();
      }).catch(function(e) {
        console.log(e);
        console.log("Error getting nb participants; see log.");
      });
    },

// REGISTER THE ADDRESSES OF THE participants
    addCheckBox: function(){
      //BUYERS
      var test = document.getElementById("0").value;
      var checkboxes = document.getElementById('checkboxes');
      var checkbox = document.createElement('input');
      checkbox.type = "radio";
      checkbox.value = document.getElementById("0").value;
      checkbox.id = document.getElementById("0").value;
      checkbox.name = "options";
      checkbox.class = "options"
      var label = document.createElement('label')
      label.htmlFor = document.getElementById("0").value;
      label.appendChild(document.createTextNode(document.getElementById("0").value));
      checkboxes.appendChild(checkbox);
      checkboxes.appendChild(label);


      // consumers
      var test2 = document.getElementById("0").value;
      var checkboxes2 = document.getElementById('checkconsumer');
      var checkbox2 = document.createElement('input');
      checkbox2.type = "checkbox";
      checkbox2.value = document.getElementById("0").value;
      checkbox2.id = document.getElementById("0").value;
      checkbox2.class = "consumer checkbox";
      checkbox2.name = "consumer";
      var label2 = document.createElement('label')
      label2.htmlFor = document.getElementById("0").value;
      label2.appendChild(document.createTextNode(document.getElementById("0").value));
      checkboxes2.appendChild(checkbox2);
      checkboxes2.appendChild(label2);
    },


// Main function that enter the address in the array of the participants to the count and initialize the input radio and checkboxes
// for the function "newexpense"
    getparticipant: function(){
      var self= this;
      var text=document.getElementById("0").value;
      var j=0;
      var ok=0;
      //MAKE sure that the value entered is an addresse of 42 characters
      if(text.length == 42){
        if(participant_array.length >= 0 ){
            for(j=0; j<=participant_array.length; j++){
                if(text == participant_array[j]){
                  ok=ok+1;
                }
          }
          // make sure via the variable ok that the address is not already entered in the array and so not already sized by the user.
          // IF OK =0 then the address was not in the array, if ok > 1 then the address is already in the array of the participants.
          if(ok <1){
              participant_array[i]=text;
              i++
              var meta;
                CheckTheBill.deployed().then(function(instance) {
                meta = instance;
                // call function form the contract to initialize the array of the contract with the value get by the interface
                return meta.AddressArray(participant_array, {from: account});
                }).then(function() {
                  console.log(participant_array.length);
                  console.log(participant_array);
                  console.log("participants registered!");
                  self.addCheckBox();
                  self.initiate_balance();
                  self.refreshparticipants();
                  document.getElementById("0").value = "";
                }).catch(function(e) {
                  console.log(e);
                  console.log("Error initiating participants");
                });
            }
            else{
              alert("you can't enter the same address twice");

              document.getElementById("0").value = "";
            }
      }// end If LENGTH>=0
      else{
      }
    }
    else{
        alert("make sure the input is 42 characters long");

      }

    },


    // SHOW The array OF participants
      showparticipant: function (){
        var self= this;
        document.getElementById("show").value = "";
        var meta;
          CheckTheBill.deployed().then(function(instance) {
          meta = instance;
          return meta.getAddressArray.call(account, {from: account});
        }).then(function(value) {
          console.log("participants showed!");
          var nbparticipants = document.getElementById("show");
          nbparticipants.innerHTML = value.valueOf();
        }).catch(function(e) {
          console.log(e);
          console.log("Error showing participants");
        });
      },

      //initialize THE BALANCES of the addresses to 50 eth.
      initiate_balance: function (){
        var self = this;
        var meta;
        var newaddr= document.getElementById("0").value;
        var init = web3.fromWei(web3.eth.getBalance(""+newaddr+""), "ether").toNumber();
        console.log(init);
        CheckTheBill.deployed().then(function(instance) {
          meta = instance;
          return meta.balances_initiate(newaddr, init, {from: account});
        }).then(function(value) {
          var balance_element = document.getElementById("balance");
          balance_element.innerHTML = value.valueOf();
        }).catch(function(e) {
          console.log(e);
        });
      },

      // RETURN the balances of a specific address.
      getbalance: function (){
        var self = this;
        var addr = document.getElementById("addr").value;
        var meta;
        CheckTheBill.deployed().then(function(instance) {
          meta = instance;
          return meta.getbalances(addr, {from: account}); // appel la fonction sendCoin du contrat
        }).then(function(value) {   //afficher l'event transfer (pour afficher les balances des participants)
          console.log(value.valueOf());
          var balance_element = document.getElementById("balance");
          balance_element.innerHTML = value.valueOf();
        }).catch(function(e) {
          console.log(e);
        });
      },



      newtransaction: function() {
        var self = this;
        var name= document.getElementById("name").value;
        var amount = parseInt(document.getElementById("amount").value);
        var buyer= document.querySelector('input[name="options"]:checked').value;
        var meta;
        CheckTheBill.deployed().then(function(instance) {
        meta = instance;
          return meta.newexpense(amount, name,buyer, {from: account});
        }).then(function(value) {
          console.log(name);
          console.log(amount);
          console.log(buyer);
          self.redistribution();
          self.summary();
        }).catch(function(e) {
          console.log(e);
        });
      },

      //  function redistribution( address _buyer, address[] _consumers, uint _amount) {
      redistribution: function() {
        var self = this;
        var amount = parseInt(document.getElementById("amount").value);
        var buyer= document.querySelector('input[name="options"]:checked').value;
        var consumers = [];
            $.each($("input[name='consumer']:checked"), function(){
                consumers.push($(this).val());
            });
        console.log(consumers.length);
        var meta;
        CheckTheBill.deployed().then(function(instance) {
        meta = instance;
          return meta.redistribution(buyer, consumers,amount, {from: account});
        }).then(function(value) {
          console.log(value.valueOf());
        }).catch(function(e) {
          console.log(e);
        });
      },


      // function to get the historic of the expenses
      summary: function() {
      var date = new Date();
      var name = new String(document.getElementById("name").value);
      var buyer = new String(document.querySelector('input[name="options"]:checked').value);
      var amount= new String(document.getElementById("amount").value);
      var summary = document.getElementById('expenses');
      var div = document.createElement('div');
      div.innerHTML = date.toDateString() + " - " + name + " - " + buyer + " - " + amount + "\n" ;

      summary.appendChild(div);
    },
};

window.addEventListener('load', function() {
  // Checking if Web3 has been injected by the browser (Mist/MetaMask)
  if (typeof web3 !== 'undefined') {
    console.warn("Using web3 detected from external source. If you find that your accounts don't appear or you have 0 MetaCoin, ensure you've configured that source properly. If using MetaMask, see the following link. Feel free to delete this warning. :) http://truffleframework.com/tutorials/truffle-and-metamask")
    // Use Mist/MetaMask's provider
    window.web3 = new Web3(web3.currentProvider);
  } else {
    console.warn("No web3 detected. Falling back to http://localhost:8545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask");
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
  }

  App.start();

});
